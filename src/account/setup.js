

const winston = require('winston');
const async = require('async');
const prompt = require('prompt');
const _ = require('lodash');
const questions = require('../../config/questions').firstAccount;

/**
 * Compares the password for the default account created.
 * Effectively asks the user to re-enter the passwords if they don't match.
 * Use recursion to call itself.
 *
 * @param {any} results The object containing the passwords to compare
 * @param {function} next The callback to call once the passwords are the same
 */
function checkPassword(results, next) {
    if (results.password !== results['password:confirmation']) {
        winston.error('Passwords do not match, please try again!');
        prompt.get(questions.password, (err, newResults) => {
            if (err) {
                next(err);
            } else {
                checkPassword(_.assign(results, newResults), next);
            }
        });
    } else {
        next(null, results);
    }
}

module.exports = (Account) => {
    /**
     * Will create a default admin account the
     * first time the server is launched.
     *
     * @param {function} callback Callback if error or once the creation is done.
     */
    function accountSetup(callback) {
        winston.info('Setting up default admin account');
        async.waterfall([
            next => prompt.get(questions, next),
            checkPassword,
            (results, next) => {
                results.valid = true;
                next(null, results);
            },
            (results, next) => {
                Account.createAccount(results, (err) => {
                    if (err && err instanceof Account.createAccount.ValidationError) {
                        winston.error('Error in fields: ');
                        winston.error(err.message);
                        return Account.setup(callback);
                    }
                    next(err);
                });
            },
        ], callback);
    }
    Account.setup = accountSetup;
};
