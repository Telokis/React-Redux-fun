const WebError = require('../api/WebError');
const nconf = require('nconf');
const inspector = require('schema-inspector');
const winston = require('winston');
const async = require('async');
const Database = require('../database');
const Hash = require('../hash');
const Schema = require('../../config/schemas').createAccount;

const CONSTANTS = nconf.get('constants');

/**
 * Error thrown when object validation fails because of schema-inspector.
 *
 * @class ValidationError
 * @extends {Error}
 */
class ValidationError extends WebError {
    /**
     * Creates an instance of ValidationError.
     * @param {String} message The message explaining the error
     *
     * @memberof ValidationError
     */
    constructor(message) {
        super(403, message);
        this.type = 'ValidationError';
    }
}

/**
 * Error representing a `Pseudo already taken`.
 *
 * @class PseudoTakenError
 * @extends {WebError}
 */
class PseudoTakenError extends WebError {
    /**
     * Creates an instance of PseudoTakenError.
     *
     * @memberof PseudoTakenError
     */
    constructor() {
        super(403, 'This pseudo is not available');
    }
}

/**
 * Error representing a `Email already taken`.
 *
 * @class EmailTakenError
 * @extends {WebError}
 */
class EmailTakenError extends WebError {
    /**
     * Creates an instance of EmailTakenError.
     *
     * @memberof EmailTakenError
     */
    constructor() {
        super(403, 'This email is not available');
    }
}

module.exports = (Account) => {
    Account.createAccount = createAccount;
    /**
     * Will create an account based on the object passed as parameter.
     *
     * @param {object} account The object representing the account to create.
     * @param {function} callback
     */
    function createAccount(account, callback) {
        winston.info('Creating account');
        async.waterfall([
            next => inspector.sanitize(Schema.sanitization, account, next),
            (_, next) => inspector.validate(Schema.validation, account, next),
            (validationResults, next) => {
                if (validationResults.valid !== true) {
                    return next(new ValidationError(validationResults.format()));
                }
                next(null, account);
            },
            (account, next) => Account.checkAvailability(account, next),
            (fields, next) => {
                if (fields.length) {
                    if (account.pseudo.toLowerCase() === fields[0].pseudo.toLowerCase()) {
                        next(new PseudoTakenError());
                    } else if (account.email.toLowerCase() === fields[0].email.toLowerCase()) {
                        next(new EmailTakenError());
                    } else {
                        // Impossible, rilly
                        winston.error('WTF, REALLY?????');
                        process.exit(1337);
                    }
                } else {
                    next(null, account);
                }
            },
            (account, next) => {
                Hash.hash(nconf.get('bcrypt_rounds'), account.password, (err, res) => next(err, account, res));
            },
            (account, bcryptedPassword, next) => {
                account.password = bcryptedPassword;
                Hash.hash(nconf.get('bcrypt_rounds'), account.secretA, (err, res) => next(err, account, res));
            },
            (account, bcryptedSecretA, next) => {
                account.secretA = bcryptedSecretA;
                account = Schema.postValidation(account);
                next(null, account);
            },
            (account, next) => {
                Database.insertRow(CONSTANTS.ACCOUNT_TABLE_NAME, account, next);
            },
        ], callback);
    }

    Account.createAccount.ValidationError = ValidationError;
};
