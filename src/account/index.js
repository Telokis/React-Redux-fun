(function expand(Account) {
    require('./setup.js')(Account);
    require('./createAccount.js')(Account);
    require('./findAccount.js')(Account);
}(module.exports));
