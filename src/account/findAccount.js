const nconf = require('nconf');
const winston = require('winston');
const async = require('async');
const Database = require('../database');

const CONSTANTS = nconf.get('constants');

module.exports = (Account) => {
    /**
     * Will try to find an account by email or pseudo.
     *
     * @param {string} emailOrPseudo Either the mail or pseudo to find
     * @param {function} callback
     */
    Account.findAccount = function findAccount(emailOrPseudo, callback) {
        winston.info('Finding account');
        async.waterfall([
            next => Database.findRows(CONSTANTS.ACCOUNT_TABLE_NAME,
                [
                    {
                        pseudo: emailOrPseudo,
                    }, {
                        email: emailOrPseudo,
                    },
                ],
                next),
            (results, fields, next) => {
                next(null, results);
            },
        ], callback);
    };

    /**
     * Will check to see if a user already exists
     *
     * @param {object} emailAndPseudo Either the mail or pseudo to find
     * @param {function} callback
     */
    Account.checkAvailability = function checkAvailability(emailAndPseudo, callback) {
        winston.info('Checking availability of account');
        async.waterfall([
            next => Database.findRows(CONSTANTS.ACCOUNT_TABLE_NAME,
                [
                    {
                        pseudo: emailAndPseudo.pseudo,
                    }, {
                        email: emailAndPseudo.email,
                    },
                ],
                next),
            (results, fields, next) => {
                next(null, results);
            },
        ], callback);
    };
};
