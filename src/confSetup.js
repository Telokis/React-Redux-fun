/* global $__dirname:false */

import path from 'path';
import nconf from 'nconf';
import winston from 'winston';

nconf.defaults({
    config        : 'config.json',
    prod          : process.env.NODE_ENV === 'production',
    dev           : process.env.NODE_ENV !== 'production',
    bcrypt_rounds : 12,
}).argv().overrides({
    base_dir  : $__dirname,
    constants : require('../config/constants'),
}).env('__');

const configFile = path.resolve(nconf.get('base_dir'), nconf.get('config'));
nconf.file(configFile);

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
    colorize: true,
    timestamp() {
        const date = new Date();
        return `${date.format('yyyy/mm/dd HH:MM:ss.l')}`;
    },
    level     : nconf.get('verbose') ? 'verbose' : 'info',
    json      : false,
    stringify : true,
});
