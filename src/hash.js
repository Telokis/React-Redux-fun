

// Thanks to NodeBB

/**
 * @module Hash
 */
(function expand(module) {
    const fork = require('child_process').fork;
    const path = require('path');
    const nconf = require('nconf');

    /**
     * Will invoke another process to hash the string.
     *
     * @function hash
     * @memberof module:Hash
     * @param {Number} rounds The number of rounds to bcrypt
     * @param {String} str The string to hash
     * @param {function} callback
     */
    module.hash = function hash(rounds, str, callback) {
        forkChild({ type: 'hash', rounds, str }, callback);
    };

    /**
     * Will invoke another process to compare the string and the hash.
     *
     * @function compare
     * @memberof module:Hash
     * @param {String} str The clear string to check
     * @param {String} hash The hash compared against
     * @param {function} callback
     */
    module.compare = function compare(str, hash, callback) {
        if (!hash || !str) {
            setImmediate(callback, null, false);
        } else {
            forkChild({ type: 'compare', str, hash }, callback);
        }
    };

    /**
     * Will fork the process to run bcrypt in another thread.
     *
     * @memberof module:Utils
     * @private
     * @param {Object} message Informations about the job to do
     * @param {function} callback
     */
    function forkChild(message, callback) {
        let forkProcessParams = {};
        if (global.v8debug || parseInt(process.execArgv.indexOf('--debug'), 10) !== -1) {
            forkProcessParams = { execArgv: [`--debug=${(5859)}`, '--nolazy'] };
        }
        const bcryptPath = path.join(nconf.get('base_dir'), 'src/bcrypt');
        const child = fork(bcryptPath, [], forkProcessParams);

        child.on('message', (msg) => {
            if (msg.err) {
                return callback(new Error(msg.err));
            }

            callback(null, msg.result);
        });

        child.send(message);
    }

    return module;
}(exports));

