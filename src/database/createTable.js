const mysql = require('mysql');
const nconf = require('nconf');
const winston = require('winston');
const _ = require('lodash');

const escape = mysql.escape.bind(mysql);
const escapeId = mysql.escapeId.bind(mysql);

module.exports = (Database) => {
    Database.createTable = databaseCreateTable;
    /**
     * Create a single table from the description passed as parameter.
     *
     * @deprecated
     * @todo Migrate to a proper sql-based format.
     * @param {String} name Name of the table
     * @param {Object} format Description of the table's columns
     * @param {function} callback
     */
    function databaseCreateTable(name, format, callback) {
        winston.verbose(`Creating table ${name}...`);
        const dbName = escapeId(nconf.get('mysql:database'));
        const tableName = escapeId(name);
        const indexes = [];
        let primary;
        const rows = _.map(format, (val, key) => {
            const escapedKey = escapeId(key);
            if (val.index === true) {
                indexes.push(escapedKey);
            }
            if (val.primary === true) {
                primary = escapedKey;
            }
            const canNull = val.canNull ? ' NULL' : ' NOT NULL';
            const def = val.default ? ` DEFAULT ${val.shouldNotEscapeDefaultValue ? val.default : escape(val.default)}` : '';
            const autoIncrement = val.autoIncrement ? ' AUTO_INCREMENT' : '';
            return `${escapedKey} ${val.type}${canNull}${autoIncrement}${def}`;
        });
        if (indexes.length) {
            rows.push(`INDEX (${indexes.join(', ')})`);
        }
        if (primary) {
            rows.push(`PRIMARY KEY (${primary})`);
        }
        const query = `CREATE TABLE IF NOT EXISTS ${dbName}.${tableName} (${rows.join(', ')}) ENGINE = InnoDB`;
        winston.verbose(`SQL> ${query}`);
        Database.pool.query(query, callback);
    }
};
