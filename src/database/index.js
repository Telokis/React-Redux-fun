(function expand(Database) {
    require('./createTable.js')(Database);
    require('./init.js')(Database);
    require('./insertRow.js')(Database);
    require('./populateDefaults.js')(Database);
    require('./queryFormat.js')(Database);
    require('./setup.js')(Database);
    require('./find.js')(Database);
}(module.exports));
