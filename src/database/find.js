const mysql = require('mysql');
const winston = require('winston');

const escapeId = mysql.escapeId.bind(mysql);

module.exports = (Database) => {
    Database.findRows = function findRows(table, values, callback) {
        const query = `SELECT * FROM ${escapeId(table)} {WHEREADV _}`;
        winston.verbose(`> SQL ${Database.queryFormat(query, values)}`);
        Database.pool.query(query, values, callback);
    };
};
