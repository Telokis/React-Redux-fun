

const mysql = require('mysql');
const nconf = require('nconf');
const winston = require('winston');
const _ = require('lodash');

module.exports = (Database) => {
    Database.createPool = databaseCreatePool;
    /**
     * Shortcut to create a mysql pool of connections.
     *
     * @param {Object} opts Additional options to add to the function.
     */
    function databaseCreatePool(opts) {
        Database.pool = mysql.createPool(_.assign({
            connectionLimit : 50,
            host            : nconf.get('mysql:host'),
            port            : nconf.get('mysql:port'),
            user            : nconf.get('mysql:username'),
            password        : nconf.get('mysql:password'),
            debug           : nconf.get('dev') ? nconf.get('mysql:debug') : false,
            queryFormat     : Database.queryFormat,
        }, opts));
    }

    Database.endPool = databaseEndPool;
    /**
     * Shortcut to end the pool and set `Database.pool` to null.
     *
     * @param {function} callback
     */
    function databaseEndPool(callback) {
        Database.pool.end((err) => {
            Database.pool = null;
            callback(err);
        });
    }

    Database.init = databaseInit;
    /**
     * Will initialize the database.
     *
     * @param {function} callback
     */
    function databaseInit(callback) {
        Database.createPool({
            database: nconf.get('mysql:database'),
        });

        // TODO : See this later
        /* if (nconf.get('dev')) {
            Database.pool.on('connection', (connection) => {
                connection.on('enqueue', (sequence) => {
                    if (sequence instanceof mysql.Sequences.Query) {
                        winston.info(sequence.sql);
                    }
                });
            });
        }*/

        Database.pool.on('error', (err) => {
            winston.error('An error occurred with the SQL Pool.');
            winston.error(err.message);
            winston.error('Shutting down...');
            process.exit(1);
        });

        callback();
    }
};
