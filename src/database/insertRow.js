const mysql = require('mysql');
const winston = require('winston');

const escapeId = mysql.escapeId.bind(mysql);

module.exports = (Database) => {
    Database.insertRow = databaseInsertRow;
    /**
     * Will insert a row inside @a table.
     *
     * @param {String} table The name of the table to insert into.
     * @param {Object} values The values to insert into the table.
     * @param {function} callback
     */
    function databaseInsertRow(table, values, callback) {
        const query = `INSERT INTO ${escapeId(table)} {VALUES _}`;
        winston.verbose(`> SQL ${Database.queryFormat(query, values)}`);
        Database.pool.query(query, values, callback);
    }
};
