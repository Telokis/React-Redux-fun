

const mysql = require('mysql');
const nconf = require('nconf');
const winston = require('winston');
const async = require('async');
const accountTable = require('../../config/tables').accountTable;

const CONSTANTS = nconf.get('constants');
const escapeId = mysql.escapeId.bind(mysql);

module.exports = (Database) => {
    Database.populateDefaults = databasePopulateDefaults;
    /**
     * Will create the default tables and fill them with the defaults values
     *
     * @param {function} callback
     */
    function databasePopulateDefaults(callback) {
        async.waterfall([
            (next) => {
                const dbName = escapeId(nconf.get('mysql:database'));
                winston.info('Creating database if not exists');
                winston.verbose(`SQL> CREATE DATABASE IF NOT EXISTS ${dbName}`);
                Database.pool.query(`CREATE DATABASE IF NOT EXISTS ${dbName}`, err => next(err));
            },
            Database.pool.end.bind(Database.pool),
            (next) => {
                winston.info('Database ok');
                winston.verbose('Switching to pool');
                Database.createPool({
                    database: nconf.get('mysql:database'),
                });
                Database.pool.getConnection(next);
            },
            (connection, next) => {
                winston.verbose('Reattempting ping.');
                connection.ping(err => next(err));
            },
            (next) => {
                Database.createTable(CONSTANTS.ACCOUNT_TABLE_NAME, accountTable, err => next(err));
            },
        ], callback);
    }
};
