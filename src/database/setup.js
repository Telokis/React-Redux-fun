

const mysql = require('mysql');
const nconf = require('nconf');
const winston = require('winston');

module.exports = (Database) => {
    Database.setup = databaseSetup;
    /**
     * Will setup the tables and default values for the database.
     *
     * @param {function} callback
     */
    function databaseSetup(callback) {
        Database.pool = mysql.createConnection({
            host        : nconf.get('mysql:host'),
            port        : nconf.get('mysql:port'),
            user        : nconf.get('mysql:username'),
            password    : nconf.get('mysql:password'),
            debug       : nconf.get('dev') ? nconf.get('mysql:debug') : false,
            queryFormat : Database.queryFormat,
        });
        winston.verbose('Attempting to ping the server...');
        Database.pool.ping((err) => {
            if (err) {
                winston.error('Unable to ping the database.');
                return callback(err);
            }
            winston.verbose('Ping OK');
            Database.populateDefaults(callback);
        });
    }
};
