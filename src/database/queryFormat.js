

const _ = require('lodash');
const mysql = require('mysql');

const escape = mysql.escape.bind(mysql);
const escapeId = mysql.escapeId.bind(mysql);

/**
 * Will join an object in the form `key = value` with proper escaping and
 * separated by @a sep.
 * @a name will be prepended to the result.
 *
 * @param {String} name The name to prepend to the result
 * @param {String} sep The separator to use for joining
 * @param {Object} obj The data to join
 * @returns  {String} The joined string
 */
function joinObject(name, sep, obj) {
    const arr = _.map(obj, (val, key) => `${escapeId(key)} = ${escape(val)}`);
    name = name ? `${name} ` : '';
    return `${name}(${arr.join(sep)})`;
}

const handlers = {
    values: (obj) => {
        const keys = _.keys(obj);
        const vals = _.map(_.values(obj), escape);
        return `(${keys.join(', ')}) VALUES (${vals.join(', ')})`;
    },
    where    : obj => joinObject('WHERE', ' AND ', obj),
    whereor  : obj => joinObject('WHERE', ' OR ', obj),
    whereadv : (obj) => {
        const vals = _.map(obj, val => joinObject(null, ' AND ', val));
        return `WHERE (${vals.join(' OR ')})`;
    },
    set    : obj => joinObject('SET', ', ', obj),
    update : obj => joinObject('UPDATE', ', ', obj),
};

/**
 * Shortcut to `Object.hasOwnProperty`.
 *
 * @param {Object} obj The object to check on.
 * @param {String} key The key to look for.
 * @returns {boolean} Whether @a obj has @a key as own property.
 */
function hasOwnProperty(obj, key) {
    return Object.prototype.hasOwnProperty.call(obj, key);
}

module.exports = (Database) => {
    Database.queryFormat = databaseQueryFormat;
    /**
     * Will replace special templated expressions with proper expressions.
     * Allows to write 'WHERE', 'SET' and other sql arguments easier.
     *
     * @param {String} query The query potentially containing the template.
     * @param {Object} values The values to integrate into the query.
     * @returns  {String} The query if no values were passed. The processed query otherwise.
     */
    function databaseQueryFormat(query, values) {
        if (!values) {
            return query;
        }
        return query.replace(/\{(.+) (.+)\}/g, (match, handler, key) => {
            handler = handler.toLowerCase();
            if (hasOwnProperty(handlers, handler)) {
                return handlers[handler](key === '_' ? values : values[key]);
            }
            return match;
        });
    }
};
