/* global _DEV_SERVER_ */

import express from 'express';
import path from 'path';
import nconf from 'nconf';
import winston from 'winston';
import fs from 'fs';
import session from 'express-session';
import compression from 'compression';
import bodyParser from 'body-parser';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import App from '../client/App';

const Api = require('./api');
// const Middlewares = require('./middlewares');

/**
 * Will setup and run the webserver.
 *
 * @export
 */
export default function () {
    const app = express();

    app.use(compression());

    app.use(session({
        secret            : nconf.get('secret'),
        resave            : false,
        saveUninitialized : true,
    }));
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    });

    Api.init(app);
    // const staticGame = express.static(path.join(nconf.get('base_dir'), '/Amet/dist'));
    // app.use('/game', Middlewares.redirectIfNotLoggedIn, staticGame);

    if (!_DEV_SERVER_) {
        const indexPath = path.join(nconf.get('base_dir'), 'dist/index.html');
        const pageContent = fs.readFileSync(indexPath, 'utf8');

        app.get('*', (req, res, next) => {
            const context = {};
            const htmlData = renderToString(
                <StaticRouter
                    location={req.url}
                    context={context}
                >
                    <App />
                </StaticRouter>
            );

            if (context.status === 404) {
                winston.info(`Passing ${req.url} along`);
                next();
            } else {
                res.send(pageContent.replace('<div id="app"></div>',
                    `<div id="app">${htmlData}</div>`));
            }
        });

        app.use(express.static(path.join(nconf.get('base_dir'), '/dist')));
    }

    const PORT = _DEV_SERVER_ ? 8081 : (nconf.get('port') || 8080);
    app.listen(PORT, () => {
        winston.info(`Express server running on port ${PORT}`);
    });
}
