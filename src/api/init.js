const express = require('express');

module.exports = (Api) => {
    /**
     * Will setup the API
     *
     * @param {any} app Express app to extend
     */
    Api.init = function init(app) {
        // https://expressjs.com/en/api.html#router
        const router = express.Router();

        require('./login')(router);
        require('./register')(router);

        app.use('/api', router);
    };
};
