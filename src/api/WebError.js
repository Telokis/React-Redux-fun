module.exports = class WebError {
    /**
     * Creates an instance of WebError.
     * @param {number} status The HTTP status associated to this error
     * @param {string} message The user friendly message to display
     */
    constructor(status, message) {
        this.message = message;
        this.status = status;
        this.error = true;
    }
};
