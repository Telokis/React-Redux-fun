const winston = require('winston');
const Account = require('../account');
const async = require('async');

module.exports = (app) => {
    app.post('/register', (req, res) => {
        winston.info('Registering!');
        const account = req.body;
        async.waterfall([
            next => Account.createAccount(account, next),
        ], (err) => {
            if (err) {
                res.json(err);
            } else {
                res.json({ redirect: '/' });
            }
        });
    });
};
