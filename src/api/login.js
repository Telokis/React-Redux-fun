const WebError = require('./WebError');
const winston = require('winston');
const Account = require('../account');
const Hash = require('../hash');
const Tools = require('../tools');
const async = require('async');

/**
 * Error representing a `Failed to login`.
 *
 * @class BadCombinationError
 * @extends {WebError}
 */
class BadCombinationError extends WebError {
    /**
     * Creates an instance of BadCombinationError.
     *
     * @memberof BadCombinationError
     */
    constructor() {
        super(403, 'Invalid combination');
    }
}

module.exports = (app) => {
    app.post('/login', (req, res) => {
        winston.info('Logging in!');
        let user;
        async.waterfall([
            next => Account.findAccount(req.body.emailOrPseudo, next),
            (fields, next) => {
                if (!fields.length) {
                    next(new BadCombinationError());
                } else {
                    user = fields[0];
                    Hash.compare(req.body.password, user.password, next);
                }
            },
            (pwdMatch, next) => {
                if (!pwdMatch) {
                    next(new BadCombinationError());
                } else {
                    req.session.user = Tools.cleanUser(user);
                    next();
                }
            },
        ], (err) => {
            if (err && err instanceof WebError) {
                res.json(err);
            } else if (err) {
                res.json(new WebError(500, 'Internal server error'));
            } else {
                res.json({ redirect: '/' });
            }
        });
    });

    app.get('/isLoggedIn', (req, res) => {
        const isLoggedIn = !!(req.session && req.session.user);
        res.json({ isLoggedIn });
    });

    app.get('/logout', (req, res) => {
        delete req.session.user;
        res.redirect('/');
    });
};
