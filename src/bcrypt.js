

// Thanks to NodeBB

const bcrypt = require('bcryptjs');
const async = require('async');

process.on('message', (msg) => {
    if (msg.type === 'hash') {
        hashStr(msg.str, msg.rounds);
    } else if (msg.type === 'compare') {
        bcrypt.compare(String(msg.str || ''), String(msg.hash || ''), done);
    }
});

/**
 * @module Bcrypt
 */

/**
 * Will generate a bcrypted hash representing the str.
 *
 * @memberof module:Bcrypt
 * @private
 * @param {String} str The string to hash.
 * @param {Number} rounds The number of rounds to do.
 */
function hashStr(str, rounds) {
    async.waterfall([
        (next) => {
            bcrypt.genSalt(parseInt(rounds, 10), next);
        },
        (salt, next) => {
            bcrypt.hash(str, salt, next);
        },
    ], done);
}

/**
 * Will send the result to the master process and end this fork.
 *
 * @memberof module:Bcrypt
 * @private
 * @param {any} err
 * @param {any} result The result of the job
 */
function done(err, result) {
    if (err) {
        process.send({ err: err.message });
        process.disconnect();
    } else {
        process.send({ result });
        process.disconnect();
    }
}
