const prompt = require('prompt');
const path = require('path');
const nconf = require('nconf');
const async = require('async');
const fs = require('fs');
const questions = require('../config/questions');
const winston = require('winston');
const Database = require('./database');
const Account = require('./account');

/**
 * @module Utils
 */

/**
 * Will create the proper object from the flat one.
 * This function is specifically made for the generated
 * config object that prompt creates.
 *
 * @memberof module:Utils
 * @private
 * @param {Object} config Contains the config for mysql
 * @returns {Object}  The unflattened hash.
 */
function splitValues(config) {
    const res = {
        mysql: {
            host     : config['mysql:host'],
            port     : config['mysql:port'],
            username : config['mysql:username'],
            password : config['mysql:password'],
            database : config['mysql:database'],
        },
        secret: config.secret,
    };

    return res;
}

/**
 * @module Setup
 */
module.exports = setupAll;

/**
 * Will ask the user for some info and then setup
 * the database and the default admin account.
 *
 * @memberof module:Setup
 * @param {function} callback
 */
function setupAll(callback) {
    prompt.start();
    prompt.get(questions.config, (err, result) => {
        if (err) {
            return callback(err);
        }
        const config = path.resolve(nconf.get('base_dir'), nconf.get('config'));

        fs.writeFile(config, JSON.stringify(splitValues(result), null, 4), (err) => {
            if (err) {
                return callback(err);
            }

            winston.info('Configuration Saved');

            nconf.file(config);

            async.series([
                Database.setup,
                Account.setup,
                next => Database.endPool(next),
            ], callback);
        });
    });
}
