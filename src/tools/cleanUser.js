const _ = require('lodash');

module.exports = hash => _.pick(hash, [
    'pseudo',
    'email',
]);
