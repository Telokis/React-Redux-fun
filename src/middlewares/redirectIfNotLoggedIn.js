module.exports = (Middlewares) => {
    Middlewares.redirectIfNotLoggedIn = function redirectIfNotLoggedIn(req, res, next) {
        if (!(req.session && req.session.user)) {
            return (res.redirect('/login'));
        }
        res.locals.user = req.session.user;
        next();
    };
};
