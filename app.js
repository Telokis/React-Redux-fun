import nconf from 'nconf';
import winston from 'winston';
import './src/confSetup';
import './external/date.format';
import Database from './src/database';
import Webserver from './src/webserver';
import setup from './src/setup';

winston.verbose(`Mode : ${nconf.get('dev') ? 'dev' : 'prod'}`);

if (nconf.get('setup') || !nconf.get('mysql:host') || !nconf.get('mysql:port')) {
    winston.info('Running setup mode');
    setup((err) => {
        if (err) {
            winston.error('Error while setting up:');
            winston.error(err.message);
            winston.error('Shutting down...');
            process.exit(1);
        }
        winston.info('Setup done, please restart.');
        process.exit(1);
    });
}

Webserver();

Database.init((err) => {
    if (err) {
        winston.error('Error setting up the database:');
        winston.error(err.message);
        process.exit(1);
    }
});
