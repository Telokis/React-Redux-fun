import 'regenerator-runtime/runtime';
import { } from 'redux-saga';
import { put, takeLatest, all, cps } from 'redux-saga/effects';
import {
    DO_RETRIEVE_LOGIN_STATUS,
    DO_LOGIN_ATTEMPT,
    LOGIN_LOADING,
} from './actions/types';
import {
    ajaxError,
    retrievedLoginStatus,
    basicAction,
    loginError,
} from './actions/creators';
import Ajax from './lib/ajax';

/**
 * Action triggering a login status retrieval.
 *
 * @export
 */
function* retrieveLoginStatus() {
    try {
        const result = yield cps(Ajax.get, Ajax.api('/isLoggedIn'));
        yield put(retrievedLoginStatus(result.isLoggedIn));
    } catch (error) {
        yield put(ajaxError(error));
    }
}

/**
 * Action triggering a login attempt.
 *
 * @export
 */
function* loginAttempt({ credentials }) {
    try {
        yield put(basicAction(LOGIN_LOADING));
        const result = yield cps(Ajax.post, Ajax.api('/login'), credentials);
        console.log('Result of logging in: ', result); // eslint-disable-line
        if (result.error && result.message) {
            yield put(loginError(result));
        }
    } catch (error) {
        yield put(ajaxError(error));
    }
}

/**
 * Watches for DO_RETRIEVE_LOGIN_STATUS
 *
 * @export
 */
function* watchRetrieveLoginStatus() {
    yield takeLatest(DO_RETRIEVE_LOGIN_STATUS, retrieveLoginStatus);
}

/**
 * Watches for DO_LOGIN_ATTEMPT
 *
 * @export
 */
function* watchLoginAttempt() {
    yield takeLatest(DO_LOGIN_ATTEMPT, loginAttempt);
}

/**
 * All watchers combined
 *
 * @export
 */
export default function* rootSaga() {
    yield all([
        watchRetrieveLoginStatus(),
        watchLoginAttempt(),
    ]);
}
