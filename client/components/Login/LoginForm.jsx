import React, { Component } from 'react';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    Alert,
    FormControl,
    Form,
    Button,
} from 'react-bootstrap';
import FA from 'react-fontawesome';
import PropTypes from 'prop-types';
import style from './Login.scss';

/**
 * Dumb component for the login form.
 *
 * @class LoginForm
 * @extends {Component}
 */
class LoginForm extends Component {
    static propTypes = {
        submitLogin : PropTypes.func.isRequired,
        loading     : PropTypes.bool,
        error       : PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.null,
        ]),
    }

    /**
     * Creates an instance of LoginForm.
     * @param {object} props Props
     * @memberof LoginForm
     */// eslint-disable-next-line
    constructor(props) {
        super(props);
        this.state = {};
        if (props.error) {
            this.state = { error: props.error };
        }
    }

    /**
     * Update state with props
     *
     * @param {object} nextProps New props
     * @memberof LoginForm
     */
    componentWillReceiveProps(nextProps) {
        if (nextProps.error) {
            this.setState({
                error: nextProps.error,
            });
        }
    }

    /**
     * Updates state with changed input.
     *
     * @param {object} event Event
     * @memberof LoginForm
     */// eslint-disable-next-line
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name] : value,
            error  : null,
        });
    }

    /**
     * Submits the login form manually
     *
     * @param {object} event Event
     * @memberof LoginForm
     */
    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            error: null,
        });
        this.props.submitLogin({
            emailOrPseudo : this.state.emailOrPseudo,
            password      : this.state.password,
        });
    }

    /**
     * Renders the whole login form
     *
     * @returns {React.Node} React node
     * @memberof LoginForm
     */
    render() {
        return (
        <Row>
        <Col md={6} mdOffset={3}>
        <div className={ style.loginContainer }>
        <h2>Login</h2>
        {
            this.state.error &&
            <Alert bsStyle="danger">
                <FA name="exclamation-triangle"/>{' '}{this.state.error}
            </Alert>
        }
        <Form onSubmit={ this.handleSubmit.bind(this) }>
            <FormGroup
                controlId="formEmailOrPseudo"
            >
                <ControlLabel>Pseudo or email</ControlLabel>
                <FormControl
                    type="text"
                    placeholder="Pseudo or email"
                    name="emailOrPseudo"
                    onChange={this.handleInputChange.bind(this)}
                />
                <FormControl.Feedback />
            </FormGroup>
            <FormGroup
                controlId="formPassword"
            >
                <ControlLabel>Password</ControlLabel>
                <FormControl
                    type="password"
                    placeholder="Password"
                    name="password"
                    onChange={this.handleInputChange.bind(this)}
                />
                <FormControl.Feedback />
            </FormGroup>
            <Button disabled={this.props.loading} bsStyle="primary" type="submit">
                Login
            </Button>
        </Form>
        </div>
        </Col>
        </Row>
        );
    }
}

export default LoginForm;
