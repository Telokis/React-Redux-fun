import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import LoginForm from './LoginForm';
import {
    loginAttempt,
} from '../../actions/creators';

/**
 * Smart component handling login form
 *
 * @class Login
 * @extends {React.Component}
 */
class Login extends React.Component {
    static propTypes = {
        isLoggedIn   : PropTypes.bool.isRequired,
        loggingIn    : PropTypes.bool.isRequired,
        loginAttempt : PropTypes.func.isRequired,
        error        : PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.null,
        ]),
    }

    /**
     * Proxies the call to loginAttempt passed in props.
     *
     * @param {object} obj The data to use for logging in
     * @memberof Login
     */// eslint-disable-next-line
    submitLogin(obj) {
        this.props.loginAttempt(obj);
    }

    /**
     * Returns the node to render
     *
     * @returns {React.Node} Node to render
     * @memberof Login
     */
    render() {
        if (this.props.isLoggedIn) {
            return (<Redirect to="/"/>);
        }
        return (
            <LoginForm
                submitLogin={this.submitLogin.bind(this)}
                loading={this.props.loggingIn}
                error={this.props.error}
            />
        );
    }
}

/**
 * Maps current state to props
 *
 * @param {object} state Current state
 * @returns {object} Props to use
 */
const mapStateToProps = state =>
({
    isLoggedIn : state.login.isLoggedIn || false,
    loggingIn  : state.login.loggingIn || false,
    error      : state.login.error || null,
});

export default connect(mapStateToProps, {
    loginAttempt,
})(Login);
