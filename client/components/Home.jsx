import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { retrieveLoginStatus } from '../actions/creators';

/**
 * Component representing the home page of the website
 *
 * @class Home
 * @extends {React.Component}
 */
class Home extends React.Component {
    /**
     * Triggers a loginStatus retrieval
     *
     * @memberof Home
     */
    componentDidMount() {
        this.props.retrieveLoginStatus();
    }

    /**
     * Renders the component
     *
     * @returns {React.Node} React node
     * @memberof Home
     */
    render() {
        return (
            <div>
                Home. Stuff should go here
            </div>
        );
    }
}

Home.propTypes = {
    isLoggedIn          : PropTypes.bool.isRequired,
    retrieveLoginStatus : PropTypes.func.isRequired,
};

/**
 * Maps state to props
 *
 * @param {object} state Current state to map
 * @returns {object} Props to use for the component
 */
const mapStateToProps = state =>
({
    isLoggedIn: state.login.isLoggedIn || false,
});

export default connect(mapStateToProps, { retrieveLoginStatus })(Home);
