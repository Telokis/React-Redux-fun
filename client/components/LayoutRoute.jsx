import React from 'react';
import _ from 'lodash';
import { Route } from 'react-router';
import PropTypes from 'prop-types';
import Layout from './Layout';

/**
 * Special Route rendering component inside the Layout.
 *
 * @param {object} props
 * @returns {JSX.Node} Route node
 */
const LayoutRoute = (props) => {
    const routeProps = _.pick(props, [
        'exact',
        'path',
        'strict',
        'computedMatch',
        'location',
    ]);
    const componentProps = _.omit(props, [
        'exact',
        'path',
        'strict',
        'computedMatch',
        'location',
    ]);
    return (<Route { ...routeProps } render={matchProps => (
        React.createElement(Layout, { ...matchProps },
            React.createElement(props.component, { ...matchProps, ...componentProps })
        )
    )} />);
};

LayoutRoute.propTypes = {
    component: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.func,
    ]),
};

export default LayoutRoute;
