import React from 'react';
import PropTypes from 'prop-types';
import {
    Grid,
} from 'react-bootstrap';
import Header from './Header';

/**
 * Defines the basic Layout for a random page
 *
 * @param {object} props
 * @return {JSX.Node} The node representing the layout
 */
const Layout = props =>
(
    <Grid>
        <Header/>
        { props.children }
    </Grid>
);

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;
