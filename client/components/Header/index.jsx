import React from 'react';
import {
    Row,
    Alert,
    Button,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import FA from 'react-fontawesome';
import { connect } from 'react-redux';
import Navbar from './Navbar';
import style from './Header.scss';
import { retrieveLoginStatus } from '../../actions/creators';

/**
 *  @brief Main app header
 */
class Header extends React.Component {

    /**
     * Retrieves the login status from the API
     *
     * @memberof Header
     */
    componentDidMount() {
        this.props.retrieveLoginStatus();
    }

    /**
     * Provides an easy way to refresh the page
     *
     * @memberof Header
     */
    refreshPage() {
        window.location.reload();
    }

    /**
     * Generates a big error message if props.globalError is set.
     *
     * @returns {React.Node} Node
     * @memberof Header
     */
    globalError() {
        if (!this.props.globalError) {
            return null;
        }
        return (
            <Alert bsStyle="danger">
                <h4>{ this.props.globalError.err }</h4>
                <p>
                    <FA spin name="cog" />
                    { ' ' }
                    { this.props.globalError.message }
                </p>
                <p>
                    <Button onClick={this.refreshPage} bsStyle="danger">Try again</Button>
                </p>
            </Alert>
        );
    }

    /**
     * Renders the Header.
     *
     * @returns {React.Node} React node
     * @memberof Header
     */
    render() {
        return (
            <div>
                <Row>
                <div className={ style.logo }>
                    Nyzia
                </div>
                { this.globalError() }
                <Navbar isLoggedIn={ this.props.isLoggedIn }/>
                </Row>
            </div>
        );
    }
}

Header.propTypes = {
    isLoggedIn          : PropTypes.bool.isRequired,
    globalError         : PropTypes.object,
    retrieveLoginStatus : PropTypes.func.isRequired,
};

/**
 * Maps state to props
 *
 * @param {object} state Current state to map
 * @returns {object} Props to use for the component
 */
const mapStateToProps = state => ({
    isLoggedIn  : state.login.isLoggedIn || false,
    globalError : state.error.global || null,
});

export default connect(mapStateToProps, {
    retrieveLoginStatus,
})(Header);
