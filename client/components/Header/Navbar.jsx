import React from 'react';
import {
    Navbar as BNavBar,
    Nav,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import NavItem from './NavItem';

/**
 * Navbar for the website
 *
 * @param {object} props React props
 * @returns {React.Node} React node
 */
const Navbar = props => (
    <BNavBar inverse fluid collapseOnSelect>
    <BNavBar.Header>
        <BNavBar.Brand>
            <Link to="/">
                Nyzia
            </Link>
        </BNavBar.Brand>
        <BNavBar.Toggle />
    </BNavBar.Header>
    <BNavBar.Collapse>
      <Nav pullRight>
        <NavItem to="/register" hide={props.isLoggedIn}>
            Register
        </NavItem>
        <NavItem to="/login" hide={props.isLoggedIn}>
            Login
        </NavItem>
      </Nav>
    </BNavBar.Collapse>
    </BNavBar>
);

Navbar.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
};

export default Navbar;
