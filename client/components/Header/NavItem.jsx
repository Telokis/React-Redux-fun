import React from 'react';
import PropTypes from 'prop-types';
import {
    NavItem as NI,
} from 'react-bootstrap';

/**
 * Simple wrapper around react-bootstrap/NavItem.
 * Allows the item to not render if props.hide is true.
 *
 * @param {object} props React props
 * @returns {React.Node} React node
 */
const NavItem = (props) => {
    const {
        hide,
        children,
        to,
    } = props;

    if (hide) {
        return null;
    }
    return (
        <NI href={ to }>
            { children }
        </NI>
    );
};

NavItem.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.func,
    ]),
    hide : PropTypes.bool,
    to   : PropTypes.string.isRequired,
};

export default NavItem;
