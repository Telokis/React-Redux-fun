import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import RegisterForm from './RegisterForm';
import {
    RegisterAttempt,
} from '../../actions/creators';

/**
 * Smart component handling Register form
 *
 * @class Register
 * @extends {React.Component}
 */
class Register extends React.Component {
    static propTypes = {
        isLoggedIn      : PropTypes.bool.isRequired,
        loggingIn       : PropTypes.bool.isRequired,
        RegisterAttempt : PropTypes.func.isRequired,
    }

    /**
     * Proxies the call to RegisterAttempt passed in props.
     *
     * @param {object} obj The data to use for logging in
     * @memberof Register
     */// eslint-disable-next-line
    submitRegister(obj) {
        this.props.RegisterAttempt(obj);
    }

    /**
     * Returns the node to render
     *
     * @returns {React.Node} Node to render
     * @memberof Register
     */
    render() {
        if (this.props.isLoggedIn) {
            return (<Redirect to="/"/>);
        }
        return (
            <RegisterForm
                submitRegister={this.submitRegister.bind(this)}
                loading={this.props.loggingIn}
            />
        );
    }
}

/**
 * Maps current state to props
 *
 * @param {object} state Current state
 * @returns {object} Props to use
 */
const mapStateToProps = state =>
({
    isLoggedIn : state.Register.isLoggedIn || false,
    loggingIn  : state.Register.loggingIn || false,
});

export default connect(mapStateToProps, { RegisterAttempt })(Register);
