import React, { Component } from 'react';
import {
    Row,
    Col,
    ControlLabel,
    FormGroup,
    FormControl,
    Form,
    Button,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import style from './Register.scss';

/**
 * Dumb component for the Register form.
 *
 * @class RegisterForm
 * @extends {Component}
 */
class RegisterForm extends Component {
    static propTypes = {
        submitRegister : PropTypes.func.isRequired,
        loading        : PropTypes.bool,
    }

    /**
     * Updates state with changed input.
     *
     * @param {object} event Event
     * @memberof RegisterForm
     */// eslint-disable-next-line
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });
    }

    /**
     * Submits the Register form manually
     *
     * @param {object} event Event
     * @memberof RegisterForm
     */
    handleSubmit(event) {
        event.preventDefault();
        this.props.submitRegister(this.state);
    }

    /**
     * Renders the whole Register form
     *
     * @returns {React.Node} React node
     * @memberof RegisterForm
     */
    render() {
        return (
        <Row>
        <Col md={6} mdOffset={3}>
        <div className={ style.RegisterContainer }>
        <h2>Register</h2>
        <Form onSubmit={ this.handleSubmit.bind(this) }>
            <FormGroup
                controlId="formEmailOrPseudo"
            >
                <ControlLabel>Pseudo or email</ControlLabel>
                <FormControl
                    type="text"
                    placeholder="Pseudo or email"
                    name="emailOrPseudo"
                    onChange={this.handleInputChange.bind(this)}
                />
                <FormControl.Feedback />
            </FormGroup>
            <FormGroup
                controlId="formPassword"
            >
                <ControlLabel>Password</ControlLabel>
                <FormControl
                    type="password"
                    placeholder="Password"
                    name="password"
                    onChange={this.handleInputChange.bind(this)}
                />
                <FormControl.Feedback />
            </FormGroup>
            <Button disabled={this.props.loading} bsStyle="primary" type="submit">
                Register
            </Button>
        </Form>
        </div>
        </Col>
        </Row>
        );
    }
}

export default RegisterForm;
