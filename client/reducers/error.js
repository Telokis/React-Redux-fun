import {
    AJAX_ERROR,
} from '../actions/types';

const defaultState = {
    global: null,
};

/**
 * Reducer updating state related to error stuff.
 *
 * @export
 * @param {object} [state=defaultState] Current state
 * @param {object} [action={}] Action data
 * @returns {object} New state
 */
export default function error(state = defaultState, action = {}) {
    switch (action.type) {
    case AJAX_ERROR:
        return {
            ...state,
            global: action.error,
        };
    default:
        return state;
    }
}
