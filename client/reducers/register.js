import {
    REGISTERN_LOADING,
} from '../actions/types';

const defaultState = {
    error: false,
};

/**
 * Reducer updating state related to register stuff.
 *
 * @export
 * @param {object} [state=defaultState] Current state
 * @param {object} [action={}] Action data
 * @returns {object} New state
 */
export default function register(state = defaultState, action = {}) {
    switch (action.type) {
    case REGISTERN_LOADING:
        return {
            ...state,
            loggingIn: action.status,
        };
    default:
        return state;
    }
}
