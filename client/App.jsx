import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import LayoutRoute from './components/LayoutRoute';
import Home from './components/Home';
import Login from './components/Login';
// import Register from './components/Register';
import root from './reducers/root';
import rootSaga from './sagas';
import * as actionCreators from './actions/creators';

const composeEnhancers = composeWithDevTools({ actionCreators });
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    root,
    composeEnhancers(
        applyMiddleware(sagaMiddleware)
    )
);

sagaMiddleware.run(rootSaga);

/**
 * Defines Routes for the application.
 *
 * @return {JSX.Node} The Switch for routing
 */
const App = () => (
    <Provider store={store}>
        <Switch>
            <LayoutRoute exact path="/" component={Home} />
            <LayoutRoute path="/login" component={Login} />
            {/* <LayoutRoute path="/register" component={Register} />*/}
            <Route render={({ staticContext }) => {
                if (staticContext) {
                    staticContext.status = 404;
                }
                return (<Redirect push to="/"/>);
            }}/>
        </Switch>
    </Provider>
);

export default App;
