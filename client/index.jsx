import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Router from './Router.jsx';

/**
 * The index of the aplication.
 * @param {any} Component The component we use to create the application.
 */
const render = (Component) => {
    ReactDOM.render((
        <AppContainer>
            <Component/>
        </AppContainer>
    ), document.getElementById('app'));
};

render(Router);
if (module.hot) {
    module.hot.accept('./Router.jsx', () => render(Router));
}
