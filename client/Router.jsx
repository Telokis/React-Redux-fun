import React from 'react';
import {
    Router as ReactRouter,
} from 'react-router-dom';
import App from './App.jsx';
import history from './lib/history';

/**
 * Main router for the app
 *
 * @class Router
 * @extends {React.Component}
 */
class Router extends React.Component {
    /**
     * Renders the component
     *
     * @returns {React.Node} Router app
     * @memberof Router
     */
    render() {
        return (
            <ReactRouter history={ history }>
                <App/>
            </ReactRouter>
        );
    }
}

export default Router;
