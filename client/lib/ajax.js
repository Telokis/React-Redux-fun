/* global _DEV_SERVER_, __DEV__SERVER_PREFIX_ */

import axios from 'axios';
import history from './history';

/**
 * Generates a response handler function for the axios promise.
 *
 * @param {function} callback
 * @returns {function} Response handler for the promise
 */
function handleResponse(callback) {
    return (response) => {
        if (response.data.redirect) {
            history.push(response.redirect);
        } else {
            callback(null, response.data, response);
        }
    };
}

/**
 * Generates a error handler function for the axios promise.
 *
 * @param {function} callback
 * @returns {function} Error handler for the promise
 */
function handleError(callback) {
    return (error) => {
        if (error.response) {
            // Not 2XX --> We still pass to callback.
            handleResponse(callback)(error.response);
        } else if (error.request) {
            callback({
                err     : 'Server unreachable',
                message : 'The server is currently unreachable.',
                status  : 0,
            });
        } else {
            callback(error);
        }
    };
}

const Ajax = {};

[
    'get',
    'delete',
    'head',
    'options',
].forEach((val) => {
    Ajax[val] = (url, callback) => {
        axios[val](url)
            .then(handleResponse(callback))
            .catch(handleError(callback));
    };
});

[
    'post',
    'put',
    'patch',
].forEach((val) => {
    Ajax[val] = (url, data, callback) => {
        axios[val](url, data)
            .then(handleResponse(callback))
            .catch(handleError(callback));
    };
});

let prefix = '/api';
if (_DEV_SERVER_) {
    prefix = __DEV__SERVER_PREFIX_;
}
Ajax.api = url => `${prefix}${url}`;

export default Ajax;
