/* global __IS_SERVER__:false */

import createHistory from 'history/createBrowserHistory';

// eslint-disable-next-line
let history = {};
if (!__IS_SERVER__) {
    history = createHistory();
}

export default history;
