import {
    LOGIN_LOADING,
    RETRIEVED_LOGIN_STATUS,
    AJAX_ERROR,
    DO_RETRIEVE_LOGIN_STATUS,
    DO_LOGIN_ATTEMPT,
    LOGIN_ERROR,
} from './types';

/**
 * Returns an action of type LOGIN_LOADING
 *
 * @export
 * @param {bool} status Whether the user is logging in or not.
 * @returns {object} LOGIN_LOADING action
 */
export function loginLoading(status) {
    return {
        type: LOGIN_LOADING,
        status,
    };
}

/**
 * Returns an action of type RETRIEVED_LOGIN_STATUS
 *
 * @export
 * @param {bool} isLoggedIn Whether the user is logged in or not.
 * @returns {object} RETRIEVED_LOGIN_STATUS action
 */
export function retrievedLoginStatus(isLoggedIn) {
    return {
        type: RETRIEVED_LOGIN_STATUS,
        isLoggedIn,
    };
}

/**
 * Returns an action of type DO_RETRIEVE_LOGIN_STATUS
 *
 * @export
 * @returns {object} DO_RETRIEVE_LOGIN_STATUS action
 */
export function retrieveLoginStatus() {
    return basicAction(DO_RETRIEVE_LOGIN_STATUS);
}

/**
 * Returns an action of type DO_LOGIN_ATTEMPT
 *
 * @export
 * @param {object} credentials User credentials
 * @returns {object} DO_LOGIN_ATTEMPT action
 */
export function loginAttempt(credentials) {
    return {
        type: DO_LOGIN_ATTEMPT,
        credentials,
    };
}

/**
 * Returns an action of type LOGIN_ERROR
 *
 * @export
 * @param {object} error Error object
 * @returns {object} LOGIN_ERROR action
 */
export function loginError(error) {
    return {
        type: LOGIN_ERROR,
        error,
    };
}

/**
 * Returns an action of type AJAX_ERROR
 *
 * @export
 * @param {object} error The ajax error
 * @returns {object} AJAX_ERROR action
 */
export function ajaxError(error) {
    return {
        type: AJAX_ERROR,
        error,
    };
}

/**
 * Helper to generate an action taking no parameters.
 *
 * @export
 * @param {string} type The type of the action
 * @returns {object} The action
 */
export function basicAction(type) {
    return { type };
}
