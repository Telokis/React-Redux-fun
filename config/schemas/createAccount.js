/**
 * Should be called once the validation has succeded.
 * Will properly update the hash to make it ok to be stored.
 *
 * @param {object} hash The object to clean
 * @returns {object} Updated hash
 */
exports.postValidation = function postValidation(hash) {
    // Should not be possible to create an account with valid = true.
    hash.valid = false;

    // We won't store this in database.
    delete hash['password:confirmation'];
    return hash;
};

exports.sanitization = {
    type       : 'object',
    strict     : true,
    properties : {
        pseudo: {
            type     : 'string',
            rules    : ['trim'],
            optional : false,
        },
        email: {
            type     : 'string',
            rules    : ['trim', 'lower'],
            optional : false,
        },
        password: {
            type     : 'string',
            optional : false,
        },
        'password:confirmation': {
            type     : 'string',
            optional : false,
        },
        valid: {
            type     : 'boolean',
            def      : false,
            optional : false,
        },
        secretQ: {
            type     : 'string',
            def      : '',
            optional : false,
        },
        secretA: {
            type     : 'string',
            def      : '',
            optional : false,
        },
    },
};

exports.validation = {
    type   : 'object',
    strict : true,
    exec   : function exec(schema, val) {
        if (val.password !== val['password:confirmation']) {
            this.report("Confirmation doesn't match password.");
        }
    },
    properties: {
        pseudo: {
            type      : 'string',
            minLength : 3,
            maxLength : 30,
            optional  : false,
            pattern   : /^[a-zA-Z0-9_-]*$/,
        },
        email: {
            type      : 'string',
            pattern   : 'email',
            maxLength : 255,
            optional  : false,
        },
        password: {
            type      : 'string',
            minLength : 6,
            maxLength : 64,
            optional  : false,
        },
        'password:confirmation': {
            type      : 'string',
            minLength : 6,
            maxLength : 64,
            optional  : false,
        },
        valid: {
            type     : 'boolean',
            optional : false,
        },
        secretQ: {
            type      : 'string',
            optional  : false,
            alias     : 'Secret question',
            minLength : 6,
        },
        secretA: {
            type      : 'string',
            optional  : false,
            alias     : 'Secret answer',
            minLength : 6,
        },
    },
};
