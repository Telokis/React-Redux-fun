const uuid = require('uuid/v4');
const nconf = require('nconf');

module.exports = [
    {
        name        : 'mysql:host',
        description : 'Host IP or address of your MySQL instance',
        default     : nconf.get('mysql:host') || '127.0.0.1',
    },
    {
        name        : 'mysql:port',
        description : 'Host port of your MySQL instance',
        default     : nconf.get('mysql:port') || 3306,
        type        : 'integer',
    },
    {
        name        : 'mysql:username',
        description : 'MySQL username',
        default     : nconf.get('mysql:username') || '',
        required    : true,
    },
    {
        name        : 'mysql:password',
        description : 'Password of your MySQL database',
        hidden      : true,
        default     : nconf.get('mysql:password') || '',
    },
    {
        name        : 'mysql:database',
        description : 'MySQL database name',
        default     : nconf.get('mysql:database') || 'DemDB',
    },
    {
        name        : 'secret',
        description : 'Secret text to use',
        default     : nconf.get('secret') || uuid(),
    },
];
