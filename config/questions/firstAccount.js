

const password = [
    {
        name        : 'password',
        description : 'Password for the account',
        hidden      : true,
        default     : '',
    },
    {
        name        : 'password:confirmation',
        description : 'Please confirm your password',
        hidden      : true,
        default     : '',
    },
];

module.exports = [
    {
        name        : 'email',
        description : 'Email for the account',
        default     : '',
        required    : true,
    },
    {
        name        : 'pseudo',
        description : 'Pseudo for the account',
        default     : 'admin',
        required    : true,
    },
].concat(password);

module.exports.password = password;
