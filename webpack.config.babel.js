import {
    WebpackShellPlugin,
    _DEV_SERVER_,
    _DEV_SERVER_FORCE,
} from './webpack/var.js';
import clientConf from './webpack/client.js';
import serverConf from './webpack/server.js';

export default () => {
    if (_DEV_SERVER_ && !_DEV_SERVER_FORCE) {
        return clientConf;
    }

    if (!_DEV_SERVER_ && _DEV_SERVER_FORCE) {
        serverConf.plugins.push(
            new WebpackShellPlugin({
                onBuildEnd: ['nodemon build/app.js --watch build'],
            })
        );
        return serverConf;
    }

    return [clientConf, serverConf];
};
