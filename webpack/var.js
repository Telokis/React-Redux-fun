const path = require('path');
const webpack = require('webpack');
const _ = require('lodash');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');
const nodeExternals = require('webpack-node-externals');
const WebpackShellPlugin = require('webpack-shell-plugin');

const _PROD_ = process.env.NODE_ENV === 'production';
const _DEV_ = !_PROD_;
const APP_DIR = path.resolve(__dirname, '..', 'client');
const SERVER_DIR = path.resolve(__dirname, '..');
const _DEV_SERVER_ = !!process.argv.find(v => v.includes('webpack-dev-server'));
const _DEV_SERVER_FORCE = process.env.NODE_ENV === 'devserver';
const __DEV__SERVER_PREFIX_ = JSON.stringify('//localhost:8081/api');
const OUT_DIR = path.resolve(__dirname, '..', 'build');

export {
    path,
    webpack,
    _,
    OUT_DIR,
    ExtractTextPlugin,
    HtmlWebpackPlugin,
    JavaScriptObfuscator,
    nodeExternals,
    WebpackShellPlugin,
    _PROD_,
    _DEV_,
    APP_DIR,
    SERVER_DIR,
    _DEV_SERVER_,
    _DEV_SERVER_FORCE,
    __DEV__SERVER_PREFIX_,
};
