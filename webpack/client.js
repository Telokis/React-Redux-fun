import {
    ExtractTextPlugin,
    _DEV_SERVER_,
    _DEV_SERVER_FORCE,
    APP_DIR,
    _DEV_,
    _PROD_,
    path,
    HtmlWebpackPlugin,
    __DEV__SERVER_PREFIX_,
    webpack,
    OUT_DIR,
    JavaScriptObfuscator,
    _,
} from './var';
import {
    globalCSSConf,
    basicCSSConf,
} from './cssConf';
import commonConf from './common';

const clientConf = _.merge({
    output: {
        path       : path.join(OUT_DIR, 'dist'),
        publicPath : '/',
        filename   : '[name].[hash].js',
    },
    entry: {
        app: [
            'react-hot-loader/patch',
            `${APP_DIR}/css/main.global.scss`,
            `${APP_DIR}/index.jsx`,
        ],
    },
    plugins: [
        new ExtractTextPlugin({
            filename : '[name].[hash].css',
            disable  : _DEV_SERVER_ || _DEV_SERVER_FORCE,
        }),
        new webpack.DefinePlugin({
            _DEV_SERVER_,
            __IS_SERVER__: JSON.stringify(false),
            __DEV__SERVER_PREFIX_,
        }),
        new HtmlWebpackPlugin({
            title    : 'Dem official website',
            template : `${APP_DIR}/index.ejs`,
        }),
    ],
}, commonConf);

clientConf.module.rules.push(...[
    // *.css => CSS Modules
    {
        test    : /\.s?css$/,
        exclude : /\.(global|min)\.s?css$/,
        use     : ['css-hot-loader'].concat(ExtractTextPlugin.extract({
            fallback : 'style-loader',
            use      : basicCSSConf,
        })),
    },
    // *.global.css => global (normal) css
    {
        test   : /\.(global|min)\.s?css$/,
        loader : ['css-hot-loader'].concat(ExtractTextPlugin.extract({
            fallback : 'style-loader',
            use      : globalCSSConf,
        })),
    },
]);

if (_DEV_) {
    clientConf.devtool = 'source-map';
}

if (_PROD_) {
    clientConf.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        }),
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.AggressiveMergingPlugin(),
    );

    clientConf.plugins.push(new JavaScriptObfuscator({
        rotateUnicodeArray : true,
        mangle             : true,
    }));
}

export default clientConf;
