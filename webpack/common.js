import fs from 'fs';
import { _PROD_, path } from './var.js';

const babelrcPath = path.resolve(__dirname, '..', '.babelrc');
const babelrc = JSON.parse(fs.readFileSync(babelrcPath, 'utf8'));

babelrc.presets[0] = ['es2015', { modules: false }];

const eslintLoader = {
    loader  : 'eslint-loader',
    options : {
        emitError     : true,
        failOnWarning : true,
        failOnError   : true,
    },
};

// js processing is the same for client and server
export default {
    devServer: {
        disableHostCheck : true,
        headers          : {
            'Access-Control-Allow-Origin': '*',
        },
    },
    module: {
        noParse : /\.min\.js/,
        rules   : [
            // *.js => babel + eslint
            {
                test    : /\.jsx?$/,
                exclude : /node_modules|external/,
                use     : [
                    {
                        loader  : 'babel-loader',
                        options : babelrc,
                    },
                    ..._PROD_ && [eslintLoader],
                ],
            },
            {
                test   : /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader : {
                    loader  : 'file-loader',
                    options : {
                        name: 'fonts/[name].[ext]',
                    },
                },
            },
        ],
    },

    resolve: { extensions: ['.jsx', '.js'] },
};
