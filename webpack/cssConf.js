import path from 'path';
import {
    _PROD_,
    _DEV_,
    APP_DIR,
} from './var.js';

const commonCSSConf = [
    {
        loader  : 'postcss-loader',
        options : {
            sourceMap: _DEV_,
        },
    },
    {
        loader  : 'sass-loader',
        options : {
            sourceMap    : _DEV_,
            compress     : _PROD_,
            data         : '@import "variables.global.scss";',
            includePaths : [
                path.join(APP_DIR, '/css'),
            ],
        },
    },
];

export const basicCSSConf = [
    {
        loader : 'css-loader',
        query  : {
            sourceMap      : _DEV_,
            importLoaders  : 2,
            modules        : true,
            minimize       : _PROD_,
            localIdentName : _PROD_
                ? '[hash:base64:5]'
                : '[path][name]--[local]--[hash:base64:5]',
        },
    },
    ...commonCSSConf,
];

export const globalCSSConf = [
    {
        loader : 'css-loader',
        query  : {
            sourceMap     : _DEV_,
            importLoaders : 2,
            modules       : false,
            minimize      : _PROD_,
        },
    },
    ...commonCSSConf,
];
