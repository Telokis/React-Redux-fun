import {
    SERVER_DIR,
    nodeExternals,
    _DEV_SERVER_FORCE,
    _DEV_,
    path,
    __DEV__SERVER_PREFIX_,
    webpack,
    OUT_DIR,
    _,
} from './var';
import {
    globalCSSConf,
    basicCSSConf,
} from './cssConf';
import commonConf from './common';

const serverConf = _.merge({
    target : 'node',
    node   : {
        __dirname  : false,
        __filename : false,
    },
    externals : [nodeExternals()],
    output    : {
        path     : path.join(OUT_DIR, ''),
        filename : '[name].js',
    },
    entry: {
        app: [
            `${SERVER_DIR}/app.js`,
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            $__dirname    : _DEV_SERVER_FORCE ? JSON.stringify(`${__dirname}/..`) : '__dirname',
            __IS_SERVER__ : JSON.stringify(true),
            _DEV_SERVER_  : _DEV_SERVER_FORCE,
            __DEV__SERVER_PREFIX_,
        }),
    ],
}, commonConf);

serverConf.module.rules.push(...[
    // *.css => CSS Modules
    {
        test    : /\.s?css$/,
        exclude : /\.(global|min)\.s?css$/,
        use     : basicCSSConf,
    },
    // *.global.css => global (normal) css
    {
        test : /\.(global|min)\.s?css$/,
        use  : globalCSSConf,
    },
]);

if (_DEV_) {
    serverConf.devtool = 'source-map';
}

export default serverConf;
